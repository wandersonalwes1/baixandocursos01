import { Grid, Button, Flex } from '@chakra-ui/react'
import CourseItem from '../components/course-item'
import Link from 'next/link'

export default function Home () {
  const courses = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  return (
    <>
      <Grid gridTemplateColumns={{ base: 'repeat(1, 1fr)', sm: 'repeat(2, 1fr)', md: 'repeat(3, 1fr)', lg: 'repeat(4, 1fr)' }} gap={6} mb={10}>
        {courses.map((course, index) => (
          <Link href="/course-item" key={index}>
            <a>
              <CourseItem />
            </a>
          </Link>
        ))}
      </Grid>

      <Flex justifyContent="center" mb={6}>
        <Button size="lg">
          Carregar mais cursos
        </Button>
      </Flex>
    </>
  )
}
