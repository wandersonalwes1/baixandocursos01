import {
  Text,
  Image,
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  Icon,
  List,
  ListItem,
  Grid,
  Link,
  Box
} from '@chakra-ui/react'

import NextLink from 'next/link'

import { FiHome } from 'react-icons/fi'

export default function CourseSingle () {
  return (
    <div>
      <Breadcrumb mb={10}>
        <BreadcrumbItem>
          <NextLink href="/">
            <BreadcrumbLink _focus={{ shadow: 'none' }}>
              <Icon as={FiHome} />
            </BreadcrumbLink>
          </NextLink>
        </BreadcrumbItem>

        <BreadcrumbItem>
          <NextLink href="#">
            <BreadcrumbLink _focus={{ shadow: 'none' }}>Programação</BreadcrumbLink>
          </NextLink>
        </BreadcrumbItem>

        <BreadcrumbItem isCurrentPage>
          <Text>Javascript Avançado</Text>
        </BreadcrumbItem>
      </Breadcrumb>

      <Text as="h2" fontSize="4xl" fontWeight="bold" mb={10}>Titulo do curso</Text>
      <div>

        <Text mb={10}>
        <Image
          src="https://images.unsplash.com/photo-1490365728022-deae76380607?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=967&q=80"
          width={500}
          height={350}
          rounded="md"
          shadow="lg"
            mb={4}
            mr={4}
          objectFit="cover"
          float="left"
        />
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus semper consequat laoreet. Maecenas ultrices interdum eros ac semper. Duis ac ligula eget ante mollis condimentum at vitae ligula. Nulla vestibulum laoreet enim, at ultricies mi scelerisque ac. Nunc sit amet auctor est, vitae ultricies nibh. Nam faucibus lectus quis lacinia iaculis. Cras sit amet tempus lorem.
        <br /><br />
        Morbi lectus purus, rutrum tempor dui vitae, vehicula sagittis justo. Proin commodo ligula a leo luctus dignissim. Ut blandit posuere cursus. Donec a nunc suscipit, blandit est eu, rutrum sem. Aliquam sed commodo leo. Aenean volutpat felis non lectus pellentesque ultrices. Praesent laoreet ultrices odio, ut bibendum ante venenatis eu. Praesent ultrices sapien sit amet libero tempus ullamcorper. Suspendisse molestie dolor sed vehicula posuere. Suspendisse finibus feugiat leo id imperdiet.
        <br /><br />
        Mauris orci odio, scelerisque ut arcu lacinia, dignissim ultrices diam. Praesent scelerisque molestie sem non malesuada. Nullam semper feugiat magna at lobortis. Nullam sollicitudin condimentum interdum. Cras in porttitor lectus, non dignissim sapien. Nam efficitur, augue in tincidunt molestie, lectus leo mollis nibh, ac tempus odio felis ut dui. Pellentesque a sapien id urna pulvinar pharetra. Pellentesque aliquam cursus felis, eget scelerisque mauris posuere posuere.
        <br /><br />
        Sed est magna, elementum in accumsan sed, sodales sit amet ipsum. Sed feugiat turpis ut dapibus tincidunt. Cras enim enim, volutpat at varius ut, ultrices et justo. Vivamus in tristique ex. Sed quis gravida libero. Cras vel laoreet mi. Aenean volutpat diam eu leo rutrum iaculis eget tincidunt libero. Etiam porta ipsum augue, nec ultricies magna tincidunt et. Nunc ut aliquam turpis, eu pellentesque libero.
        </Text>
        </div>

      <Grid
        templateColumns={{ base: 'repeat(1, 1fr)', sm: 'repeat(2, 1fr)' }}
        gap={6}
      >
        <div>
          <Text
            fontWeight="black"
            fontSize="lg" mb={4}
            textShadow="outline"
          >
            INFORMAÇÕES
          </Text>

          <List spacing={1}>
            <ListItem><strong>Gênero</strong>: Post</ListItem>
            <ListItem><strong>Ano de Lançamento</strong>: 2021</ListItem>
            <ListItem><strong>Formato</strong>: Texto</ListItem>
            <ListItem><strong>Idioma</strong>: Português</ListItem>
            <ListItem><strong>Tamanho</strong>: 0.00 GB</ListItem>
            <ListItem><strong>Servidor</strong>: Torrent</ListItem>
          </List>
        </div>
        <div>
          <Text fontWeight="black" fontSize="lg" mb={4}>
            DOWNLOAD
          </Text>

          <Link
            mb={4}
            fontWeight="bold"
            fontSize="lg"
            bgGradient="linear(to-l, #88AA50,#479BCA)"
            bgClip="text"
          >
            MAGNET LINK
          </Link>

          <Box bg="red.300" p={4} rounded="md" mt={4}>
            <Text fontSize="sm"><strong>AVISO</strong>: Não hospedamos nenhum arquivo em nossos servidores, todos os links aqui disponibilizados são encontrados em outros sites na internet, por esse motivo, não nos responsabilizamos por nenhum link encontrado aqui. Semeie para o próximo assim como você gostaria que estivesse disponível quando fosse baixar, pois por não termos os arquivos, torrent “MORTO” não será “RESSUSCITADO”, caso esses arquivos sejam de sua propriedade intelectual, entre em contato (ver detalhes na página “Termos de uso / Privacidade” disponível no menu superior).</Text>
          </Box>
        </div>
      </Grid>
    </div>
  )
}
