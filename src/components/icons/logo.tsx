import { useColorModeValue } from '@chakra-ui/react'

export default function Logo() {
  const color1 = useColorModeValue('#0F88DF', '#65711F')
  const color2 = useColorModeValue('#D20B6B', '#19C2E7')

  return (
    <svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
      <rect x="1" y="1" width="28" height="28" rx="3" stroke="url(#paint0_linear)" strokeWidth="2" />
      <rect x="22" y="22" width="15" height="15" rx="4" fill="white" />
      <rect x="23" y="23" width="13" height="13" rx="3" stroke="url(#paint1_linear)" strokeWidth="2" />
      <path
        d="M20.7458 13.6723C21.3883 14.0569 21.3958 14.9853 20.7594 15.3801L13.1401 20.1073C12.477 20.5188 11.6192 20.0459 11.6129 19.2656L11.5384 9.93337C11.5322 9.15301 12.3824 8.66656 13.052 9.06734L20.7458 13.6723Z"
        stroke="url(#paint2_linear)"
        strokeWidth="2"
      />
      <line x1="25.7071" y1="30.2929" x2="29.2426" y2="33.8284" stroke="url(#paint3_linear)" strokeWidth="2" />
      <line x1="29.2929" y1="33.7929" x2="32.8284" y2="30.2574" stroke="url(#paint4_linear)" strokeWidth="2" />
      <line x1="29" y1="34" x2="29" y2="27" stroke="url(#paint5_linear)" strokeWidth="2" />
      <defs>
        <linearGradient id="paint0_linear" x1="15" y1="0" x2="15" y2="30" gradientUnits="userSpaceOnUse">
          <stop stopColor={color1} />
          <stop offset="1" stopColor={color2} stopOpacity="0.5" />
        </linearGradient>
        <linearGradient id="paint1_linear" x1="29.5" y1="22" x2="29.5" y2="37" gradientUnits="userSpaceOnUse">
          <stop stopColor={color1} />
          <stop offset="1" stopColor={color2} stopOpacity="0.5" />
        </linearGradient>
        <linearGradient
          id="paint2_linear"
          x1="24.0753"
          y1="14.4997"
          x2="6.07585"
          y2="14.6434"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor={color1} />
          <stop offset="1" stopColor={color2} stopOpacity="0.5" />
        </linearGradient>
        <linearGradient
          id="paint3_linear"
          x1="26.7678"
          y1="32.7678"
          x2="26.0607"
          y2="33.4749"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor={color1} />
          <stop offset="1" stopColor={color2} stopOpacity="0.5" />
        </linearGradient>
        <linearGradient
          id="paint4_linear"
          x1="31.7678"
          y1="32.7322"
          x2="32.4749"
          y2="33.4393"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor={color1} />
          <stop offset="1" stopColor={color2} stopOpacity="0.5" />
        </linearGradient>
        <linearGradient id="paint5_linear" x1="30" y1="30.5" x2="31" y2="30.5" gradientUnits="userSpaceOnUse">
          <stop stopColor={color1} />
          <stop offset="1" stopColor={color2} stopOpacity="0.5" />
        </linearGradient>
      </defs>
    </svg>
  )
}
