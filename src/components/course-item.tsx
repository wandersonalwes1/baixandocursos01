import { chakra, Flex, Text, Image, useColorModeValue } from '@chakra-ui/react'

export default function CourseItem() {
  const colors = {
    category: {
      bg: useColorModeValue('blue.100', 'gray.700'),
      text: useColorModeValue('blue.500', 'gray.300')
    },
    date: {
      bg: useColorModeValue('gray.200', 'gray.700'),
      text: useColorModeValue('gray.500', 'gray.300')
    }
  }
  return (
    <chakra.article _hover={{ opacity: 0.7 }}>
      <Image
        src="https://images.unsplash.com/photo-1490365728022-deae76380607?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=967&q=80"
        width={500}
        height={200}
        rounded="md"
        shadow="lg"
        mb={3}
        objectFit="cover"
      />
      <Flex justifyContent="space-between" mb={4}>
        <Text bg={colors.category.bg} color={colors.category.text} px={2} py={0} rounded="md">
          categoria
        </Text>
        <Text bg={colors.date.bg} color={colors.date.text} px={2} py={0} rounded="md">
          25.06.2012
        </Text>
      </Flex>

      <Text as="h2" fontWeight="bold" fontSize="md">
        Lorem Ipsum is simply dummy text of the printing
      </Text>
    </chakra.article>
  )
}
