import { chakra, Container, Flex, Text } from '@chakra-ui/react'

export default function Footer () {
  return (
    <chakra.footer borderTopWidth="1px" py={4} mt={10}>
      <Container maxW="5xl">
        <Flex justifyContent="space-between">
          <Text>Baixando Cursos © 2021 - O melhor site de download de cursos</Text>
          <Text fontWeight="extrabold" ml={3}>iSITES</Text>
        </Flex>
      </Container>
    </chakra.footer>
  )
}
