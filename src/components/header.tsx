import {
  chakra,
  Container,
  Input,
  Text,
  Flex,
  IconButton,
  InputGroup,
  InputLeftElement,
  useColorMode,
  useColorModeValue,
  VisuallyHidden,
  Box,
  useDisclosure
} from '@chakra-ui/react'
import { RiMenu5Fill } from 'react-icons/ri'
import { FiSearch, FiSun, FiMoon, FiX } from 'react-icons/fi'

import NextLink from 'next/link'
import Logo from './icons/logo'
import LogoWithName from './icons/logo-with-name'

export default function Header() {
  const { colorMode, toggleColorMode } = useColorMode()
  const { isOpen, onToggle } = useDisclosure()
  const bg = useColorModeValue('white', 'gray.700')
  const bgInput = useColorModeValue('gray.100', 'gray.900')
  const bgMenu = useColorModeValue('white', 'gray.700')

  return (
    <chakra.header bg={bg} py={4} mb={10} shadow="sm" pos="relative">
      <Container maxW="5xl" display="flex" justifyContent="space-between" alignItems="center">
        <Flex alignItems="center" flexShrink={0}>
          <NextLink href="/">
            <chakra.a display={{ base: 'none', sm: 'inline-block' }}>
              <LogoWithName />
            </chakra.a>
          </NextLink>

          <NextLink href="/">
            <chakra.a display={{ base: 'inline-block', sm: 'none' }}>
              <Logo />
            </chakra.a>
          </NextLink>

          <VisuallyHidden>
            <Text as="h1">Baixando Cursos</Text>
          </VisuallyHidden>
        </Flex>
        <InputGroup mx={10} size="lg" display={{ base: 'none', sm: 'inline-block' }}>
          <InputLeftElement pointerEvents="none" children={<FiSearch />} />
          <Input placeholder="Digite o curso aqui" bg={bgInput} />
        </InputGroup>
        <Flex alignItems="center">
          <IconButton
            icon={colorMode === 'light' ? <FiMoon /> : <FiSun />}
            aria-label="Toggle Theme"
            size="md"
            variant="ghost"
            fontSize="24px"
            onClick={toggleColorMode}
            rounded="md"
            _focus={{
              shadow: 'none'
            }}
          />

          <IconButton
            onClick={onToggle}
            icon={isOpen ? <FiX /> : <RiMenu5Fill />}
            aria-label="Open Menu"
            variant="ghost"
            size="md"
            fontSize="24px"
            rounded="md"
            _focus={{
              shadow: 'none'
            }}
          />
        </Flex>
      </Container>

      {isOpen && (
        <Box
          bg={bgMenu}
          pos="absolute"
          top="80px"
          left="0"
          right="0"
          w="full"
          maxW="5xl"
          mx="auto"
          p={4}
          borderBottomRadius="4px"
          shadow="md"
          zIndex="10">
          <Flex flexDir="column" justifyContent="center" alignItems="center">
            <Text as="a" href="#" px={3} py={1}>
              Home
            </Text>
            <Text as="a" href="#" px={3} py={1}>
              Envie seu link
            </Text>
            <Text as="a" href="#" px={3} py={1}>
              Contato
            </Text>
            <Text as="a" href="#" px={3} py={1}>
              Termos de uso / Privacidade
            </Text>
          </Flex>
        </Box>
      )}
    </chakra.header>
  )
}
